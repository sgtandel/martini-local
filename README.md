# README #

This repository contains patches for Martini board. These patches must be used
with Amlogic's Jan 2018 release from the following link:
openlinux2.amlogic.com:8000/download/ARM/filesystem/Linux_BSP/buildroot_openlinux_kernel_4.9_20180131.tar.gz

### Build ###

1. Download Amlogic's Jan 2018 buildroot release from folowing web link and extract into $HOME directory
   openlinux2.amlogic.com:8000/download/ARM/filesystem/Linux_BSP/buildroot_openlinux_kernel_4.9_20180131.tar.gz
   
2. Apply all the patches using patch command

	# patch -p1 <patch name>
	
3. Go into buildroot_openlinux_kernel_4.9_20180131 directory

	# cd buildroot_openlinux_kernel_4.9_20180131
	
4. Run build command

	# source runCompile.sh
		
5. After sucessful build, images are created in following path

	$/HOME/buildroot_openlinux_kernel_4.9_20180131/output/mesonaxg_s420_32_debug/images
